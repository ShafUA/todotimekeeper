export const DO = {
  ADD: Symbol("create task"),
  TOGGLE: Symbol("toggle task"),
  DELETE: Symbol("delete task"),
  START: Symbol("start time count for specific task"),
  STOP: Symbol("stop time count"),
  TASK_MOVE: Symbol("move task")
};
