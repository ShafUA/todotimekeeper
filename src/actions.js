import uuid from "uuid";
import { DO } from "./consts";

export const addTask = text => {
  return {
    type: DO.ADD,
    id: uuid(),
    text
  };
};

export const toggleTask = id => {
  return {
    type: DO.TOGGLE,
    id,
    time: new Date()
  };
};

export const deleteTask = id => {
  return {
    type: DO.DELETE,
    id
  };
};

export const startTimeCount = id => {
  return {
    type: DO.START,
    id,
    time: new Date()
  };
};

export const stopTimeCount = id => {
  return {
    type: DO.STOP,
    id,
    time: new Date()
  };
};

export const moveTask = (dragIndex, hoverIndex) => {
  return {
    type: DO.TASK_MOVE,
    dragIndex,
    hoverIndex
  };
};
