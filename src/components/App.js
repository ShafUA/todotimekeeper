import React, { Component } from "react";
import { connect } from "react-redux";

import { DragDropContext } from "react-dnd";
import HTML5Backend from "react-dnd-html5-backend";

import AddField from "./AddField";
import Task from "./Task";
import { addTask, toggleTask, deleteTask, moveTask } from "../actions";
import "../styles/App.css";

class App extends Component {
  render() {
    let {
      addTask,
      tasksList,
      tasksStore,
      toggleTask,
      deleteTask,
      moveTask
    } = this.props;
    console.log("render asksList", tasksList);
    return (
      <div className="App">
        <AddField addTask={addTask} />
        {tasksList.map((task, index) => (
          <Task
            key={tasksStore[task].id}
            id={tasksStore[task].id}
            deleteTask={deleteTask}
            toggleTask={toggleTask}
            moveTask={moveTask}
            index={index}
            {...tasksStore[task]}
          />
        ))}
      </div>
    );
  }
}

export default connect(
  state => ({
    tasksList: state.tasksList,
    tasksStore: state.tasksStore
  }),
  {
    addTask,
    toggleTask,
    deleteTask,
    moveTask
  }
)(DragDropContext(HTML5Backend)(App));
