import React, { PropTypes } from "react";

class AddField extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: ""
    };
  }
  changehandler = event => {
    this.setState({ value: event.target.value });
  };
  render() {
    return (
      <div>
        <form
          onSubmit={e => {
            e.preventDefault();
            this.props.addTask(this.state.value);
            this.setState({ value: "" });
          }}
        >
          <label>
            New task:
            <input
              type="text"
              placeholder="type text of task"
              value={this.state.value}
              onChange={this.changehandler}
            />
          </label>
          <input type="submit" value="Submit" />
        </form>
      </div>
    );
  }
}

AddField.propTypes = {
  addTask: PropTypes.func.isRequired
};

export default AddField;
