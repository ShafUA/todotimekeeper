import React, { PropTypes } from "react";
import { connect } from "react-redux";

import { startTimeCount, stopTimeCount } from "../actions";

const propTypes = {
  id: PropTypes.string.isRequired,
  time: PropTypes.number,
  done: PropTypes.bool.isRequired
};

class TimeKeeper extends React.Component {
  constructor(props) {
    super(props);
    let time = this.props.time || 0;
    let seconds = Math.floor(time / 1000 % 60),
      minutes = Math.floor(time / (1000 * 60) % 60);
    this.state = {
      minutes,
      seconds
    };
    this.interval = null;
  }
  componentWillUnmount() {
    clearInterval(this.interval);
  }
  componentWillReceiveProps(nextProps) {
    if (this.interval && nextProps.currentId !== this.props.id)
      this.clockStop();
  }
  clockStart = () => {
    if (this.interval) return;
    this.interval = window.setInterval(
      () => {
        let { minutes, seconds } = this.state;
        if (seconds >= 59) {
          minutes++;
          seconds = 0;
        } else
          seconds++;
        this.setState({
          minutes,
          seconds
        });
      },
      1000
    );
    this.props.startTimeCount(this.props.id);
  };
  clockStop = () => {
    clearInterval(this.interval);
    this.interval = null;
  };
  onStopClick = () => {
    this.clockStop();
    this.props.stopTimeCount(this.props.id);
  };
  render() {
    let { minutes, seconds } = this.state;
    return (
      <div className={`time_keeper${this.interval ? " current" : ""}`}>
        <div>
          {minutes}:
          {seconds > 9 ? seconds : "0" + seconds}
        </div>

        {this.props.done
          ? ""
          : this.interval
              ? <button onClick={this.onStopClick}>Stop</button>
              : <button onClick={this.clockStart}>Start</button>}
      </div>
    );
  }
}

TimeKeeper.propTypes = propTypes;

export default connect(
  state => ({
    currentId: state.currentTask
  }),
  {
    startTimeCount,
    stopTimeCount
  }
)(TimeKeeper);
