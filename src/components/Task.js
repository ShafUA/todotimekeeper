import React, { PropTypes } from "react";
import { findDOMNode } from "react-dom";
import { DragSource, DropTarget } from "react-dnd";

import TimeKeeper from "./TimeKeeper";

const propTypes = {
  id: PropTypes.string.isRequired,
  spendMilliseconds: PropTypes.number,
  index: PropTypes.number.isRequired,
  done: PropTypes.bool.isRequired,
  deleteTask: PropTypes.func.isRequired,
  toggleTask: PropTypes.func.isRequired,
  moveTask: PropTypes.func.isRequired,
  text: PropTypes.string.isRequired
};

const taskSource = {
  beginDrag(props) {
    return {
      id: props.id,
      index: props.index
    };
  }
};

const taskTarget = {
  hover(props, monitor, component) {
    const dragIndex = monitor.getItem().index;
    const hoverIndex = props.index;

    if (dragIndex === hoverIndex) {
      return;
    }

    // Determine rectangle on screen
    const hoverBoundingRect = findDOMNode(component).getBoundingClientRect();

    // Get vertical middle
    const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;

    // Determine mouse position
    const clientOffset = monitor.getClientOffset();

    // Get pixels to the top
    const hoverClientY = clientOffset.y - hoverBoundingRect.top;

    // Only perform the move when the mouse has crossed half of the items height
    // When dragging downwards, only move when the cursor is below 50%
    // When dragging upwards, only move when the cursor is above 50%

    // Dragging downwards
    if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
      return;
    }

    // Dragging upwards
    if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
      return;
    }

    // Time to actually perform the action
    props.moveTask(dragIndex, hoverIndex);

    // Note: we're mutating the monitor item here!
    // Generally it's better to avoid mutations,
    // but it's good here for the sake of performance
    // to avoid expensive index searches.
    monitor.getItem().index = hoverIndex;
  }
};

function collect(connect, monitor) {
  return {
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging()
  };
}

const Task = (
  {
    id, done, text,
    spendMilliseconds, deleteTask, toggleTask,
    connectDragSource, connectDropTarget,  isDragging
  }
) => {
  return connectDragSource(
    connectDropTarget(
      <div
        className={`task${done ? " done" : ""}`}
        style={{ opacity: +!isDragging }}
      >
        <p>{text}</p>

        <TimeKeeper id={id} time={spendMilliseconds} done={done} />

        <button className="toggle" onClick={() => toggleTask(id)}>
          {done ? "Restore" : "Done!"}
        </button>

        <button className="delete" onClick={() => deleteTask(id)}>
          delete
        </button>

      </div>
    )
  );
};

Task.propTypes = propTypes;

export default DropTarget("task", taskTarget, connect => ({
  connectDropTarget: connect.dropTarget()
}))(DragSource("task", taskSource, collect)(Task));
