/*eslint no-unused-vars: 0*/
/*eslint no-fallthrough: 0*/

import { DO } from "./consts";

export default (
  state = {
    tasksList: [],
    tasksStore: {},
    currentTask: null,
    taskStartTime: null
  },
  action
) => {
  return timeKeeperReducer(taskReducer(state, action), action);
};

function timeKeeperReducer(state, action) {
  switch (action.type) {
    case DO.TOGGLE:
    case DO.STOP:
      if (state.currentTask !== action.id) return state;
      return {
        ...state,
        currentTask: null,
        taskStartTime: null,
        tasksStore: {
          ...state.tasksStore,
          [state.currentTask]: saveTaskTime(
            state.tasksStore[state.currentTask],
            state.taskStartTime,
            action.time
          )
        }
      };
    case DO.START:
      return {
        ...state,
        currentTask: action.id,
        taskStartTime: action.time,
        tasksStore: {
          ...state.tasksStore,
          [state.currentTask]: saveTaskTime(
            state.tasksStore[state.currentTask],
            state.taskStartTime,
            action.time
          )
        }
      };
    default:
      return state;
  }
}

function taskReducer(state, action) {
  switch (action.type) {
    case DO.ADD:
      return {
        ...state,
        tasksList: [...state.tasksList, action.id],
        tasksStore: {
          ...state.tasksStore,
          [action.id]: {
            id: action.id,
            text: action.text,
            done: false,
            spendMilliseconds: 0
          }
        }
      };

    case DO.TOGGLE:
      return {
        ...state,
        tasksStore: {
          ...state.tasksStore,
          [action.id]: {
            ...state.tasksStore[action.id],
            done: !state.tasksStore[action.id].done
          }
        }
      };

    case DO.DELETE:
      let { [action.id]: deletedTask, ...tasks } = state.tasksStore;
      return {
        ...state,
        tasksList: state.tasksList.filter(id => id !== action.id),
        tasksStore: {
          ...tasks
        }
      };

    case DO.TASK_MOVE:
      let tasksList = state.tasksList;
      let dragCard = tasksList[action.dragIndex];
      let newTasksList = [...tasksList];
      newTasksList.splice(action.dragIndex, 1);
      newTasksList.splice(action.hoverIndex, 0, dragCard);
      return {
        ...state,
        tasksList: newTasksList
      };

    default:
      return state;

  }
}

function saveTaskTime(task, startTime, endTime) {
  if (!task) return;
  return {
    ...task,
    spendMilliseconds: task.spendMilliseconds + (endTime - startTime)
  };
}
